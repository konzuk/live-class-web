

import { OnDestroy, OnInit, Component } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';
import { BaseViewComponent } from 'src/app/components/BaseViewComponent';
import { DashboardService } from 'src/app/components/dashboard/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.template.html'
})
export class DashboardComponent extends BaseViewComponent implements OnDestroy, OnInit {

  private _key: String;
  content: String;

  public constructor(private ser: DashboardService, myEvent: MyEventService) {
    super(myEvent);
  }

  key(): String { return this._key; }


  public ngOnInit(): any {
    this._key = this.ser.getData();

  }

  public ngOnReInit() {

    this._key = this.ser.getData();
  }



  public ngOnDestroy(): any {
  }

  handleData(data) {
    this.content = data.message;
  }

}
