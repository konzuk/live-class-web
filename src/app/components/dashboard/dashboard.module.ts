import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from 'node_modules/@angular/router';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule
  ],
  exports: [
    DashboardComponent
  ],
})
export class DashboardModule { }
