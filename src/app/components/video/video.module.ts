import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoListComponent } from 'src/app/components/video/video-list/video-list.component';
import { VideoCommentComponent } from 'src/app/components/video/video-comment/video-comment.component';
import { VideoRelateComponent } from 'src/app/components/video/video-relate/video-relate.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    VideoListComponent,
    VideoCommentComponent,
    VideoRelateComponent
  ],
  exports: [
    VideoListComponent,
    VideoCommentComponent,
    VideoRelateComponent
  ]
})
export class VideoModule { }
