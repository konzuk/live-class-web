import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { BsDropdownModule } from 'ngx-bootstrap';
import { BasicLayoutComponent } from 'src/app/components/common/layouts/basicLayout.component';
import { FooterComponent } from 'src/app/components/common/footer/footer.component';
import { BlankLayoutComponent } from 'src/app/components/common/layouts/blankLayout.component';
import { NavigationComponent } from 'src/app/components/common/navigation/navigation.component';
import { TopNavbarComponent } from 'src/app/components/common/topnavbar/topnavbar.component';




@NgModule({
  declarations: [
    FooterComponent,
    BasicLayoutComponent,
    BlankLayoutComponent,
    NavigationComponent,
    TopNavbarComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    BsDropdownModule.forRoot()
  ],
  exports: [
    FooterComponent,
    BasicLayoutComponent,
    BlankLayoutComponent,
    NavigationComponent,
    TopNavbarComponent
  ],
})

export class LayoutsModule { }
