import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.template.html'
})




export class NavigationComponent implements AfterViewInit {

  constructor(private router: Router) {

  }

  private menuList = [{
    description: 'វិញ្ញាសា សញ្ញាបត្រទុតិយភូមិ',
    url: 'bacii',
    subject: [
      { description: 'អក្សរសាស្រ្តខ្មែរ', url: 'k' },
      { description: 'រូបវិទ្យា', url: 'p' },
      { description: 'គណិតវិទ្យា', url: 'm' },
      { description: 'គីមីវិទ្យា', url: 'c' },
      { description: 'ជីវវិទ្យា', url: 'b' }
    ]
  },
  {
    description: 'វិញ្ញាសា ពេទ្យ',
    url: 'doctor',
    subject: [
      { description: 'គណិតវិទ្យា', url: 'm' },
      { description: 'គីមីវិទ្យា', url: 'c' },
      { description: 'ជីវវិទ្យា', url: 'b' }
    ]
  }];



  ngAfterViewInit() {
    jQuery('#side-menu').metisMenu();
    if (jQuery('body').hasClass('fixed-sidebar')) {
      jQuery('.sidebar-collapse').slimscroll({
        height: '100%'
      });
    }
  }

  activeRoute(routename: string): boolean {
    return this.router.url.indexOf(routename) > -1;
  }
}

