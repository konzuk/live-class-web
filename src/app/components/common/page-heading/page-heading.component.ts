import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-page-heading',
  templateUrl: './page-heading.component.html',
  styleUrls: ['./page-heading.component.css']
})
export class PageHeadingComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  parent;
  current;
  ngOnInit() {
    const myData = this.route.snapshot.data;
    if (myData) {
      this.parent = myData.parent;
      this.current = myData.current;

    }
  }

}
