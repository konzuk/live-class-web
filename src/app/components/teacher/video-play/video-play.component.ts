import { Component, OnInit, AfterViewInit } from '@angular/core';
import { correctHeight } from 'src/app/app.helpers';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeInRight } from 'ng-animate';

@Component({
  selector: 'app-video-play',
  templateUrl: './video-play.component.html',
  styleUrls: ['./video-play.component.css'],
  animations: [
    trigger('fadeInRight', [transition('* => *', useAnimation(fadeInRight, {
      params: {
        timing: 1,
        a: '20px',
        b: '0px',
      }
    }))]),
  ],
})
export class VideoPlayComponent implements OnInit, AfterViewInit {

  fadeInRight: any;
  dangerousVideoUrl = '';
  videoUrl: SafeResourceUrl;
  changeLocation: any;


  ngAfterViewInit(): void {
    correctHeight();
  }
  constructor(private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.dangerousVideoUrl = 'https://player.vimeo.com/video/280659641?autoplay=1';
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.dangerousVideoUrl);
  }

}
