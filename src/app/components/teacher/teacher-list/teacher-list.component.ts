import { Component, OnInit } from '@angular/core';
import { MyEventService } from 'src/app/services/my-event.service';
import { trigger, transition, animate, useAnimation } from '@angular/animations';
import { fadeInRight, fadeIn, fadeOut } from 'ng-animate';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  animations: [
    trigger('changeLocation', [transition('* => *', useAnimation(fadeInRight, {
      params: {
        timing: 1,
        a: '20px',
        b: '0px',
      }
    }))]),
    trigger('ChangeLocationInside', [
      transition((fromState, toState) => fromState === 'close' && toState === 'open', useAnimation(fadeIn))
    ])
  ],
  styleUrls: ['./teacher-list.component.css']
})

export class TeacherListComponent implements OnInit {

  constructor(private myEvent: MyEventService) { }

  gridClass = 'col-sm-6 col-md-4 col-lg-3';
  listClass = 'col-12';
  currentClass = this.gridClass;
  changeLocation: any;
  ChangeLocationInside = 'close';
  itemClassImg: String;
  itemClassTitle: String;


  playVideo(videoId) {
    this.myEvent.playVideo.next(videoId);
    this.currentClass = this.listClass;
    this.ChangeLocationInside = 'open';
    this.itemClassImg = 'col-xl-12 col-sm-3 col-4';
    this.itemClassTitle = 'col-xl-12 col-sm-9 col-8';
  }
  ngOnInit() {
    this.currentClass = this.gridClass;
  }

}

declare var jQuery: any;
