import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Globals } from 'src/app/app.globals';
import { Router } from '@angular/router';
import { MyEventService } from 'src/app/services/my-event.service';
import { LoginService } from 'src/app/components/login/login.service';
import { MyRouterService } from 'src/app/services/my-router.service';
import { BasicLayoutComponent } from 'src/app/components/common/layouts/basicLayout.component';
import { TeacherComponent } from 'src/app/components/teacher/teacher.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy, OnInit {
  constructor(
    private myEvent: MyEventService,
    private spinner: NgxSpinnerService,
    private login: LoginService,
    private globalVar: Globals,
    private myRouter: MyRouterService,
    private router: Router
  ) {
    this.prepareRoute(this.menuList);
  }


  private menuList = [{
    description: 'វិញ្ញាសា សញ្ញាបត្រទុតិយភូមិ',
    url: 'bacii',
    subject: [
      { description: 'អក្សរសាស្រ្តខ្មែរ', url: 'k' },
      { description: 'រូបវិទ្យា', url: 'p' },
      { description: 'គណិតវិទ្យា', url: 'm' },
      { description: 'គីមីវិទ្យា', url: 'c' },
      { description: 'ជីវវិទ្យា', url: 'b' }
    ]
  },
  {
    description: 'វិញ្ញាសា ពេទ្យ',
    url: 'doctor',
    subject: [
      { description: 'គណិតវិទ្យា', url: 'm' },
      { description: 'គីមីវិទ្យា', url: 'c' },
      { description: 'ជីវវិទ្យា', url: 'b' }
    ]
  }];

  ngOnInit(): void {

    this.myEvent.ToogleSpinnerEvent.subscribe(show => {
      if (show) {
        this.globalVar.isServerBusy = true;
        setTimeout(() => {
          if (this.globalVar.isServerBusy) {
            this.spinner.show();
          }
        }, 1000);
      } else {
        this.globalVar.isServerBusy = false;
        this.spinner.hide();
      }
    });

    this.myEvent.RouterNavigateEndEvent.subscribe(url => {
      this.globalVar.previousUrl = this.globalVar.currentUrl;
      this.globalVar.currentUrl = url;
    });

    this.myEvent.CallRefreshTokenEvent.subscribe(() => {
      this.login.refreshTokenOauth();
    });
  }
  ngOnDestroy(): void {

    this.myEvent.ToogleSpinnerEvent.unsubscribe();
    this.myEvent.RouterNavigateEndEvent.unsubscribe();
    this.myEvent.CallRefreshTokenEvent.unsubscribe();
  }

  private prepareRoute(menuList) {

    this.router.config.pop();

    menuList.forEach(menu => {

      const parent = {
        path: menu.url, component: BasicLayoutComponent,
        children: []
      };

      menu.subject.forEach(sub => {
        parent.children.push({
          path: sub.url, component: TeacherComponent, data: {
            shouldDetach: true,
            parentKey: menu.url,
            parent: menu,
            current: sub
          }
        });
      });
      this.router.config.push(parent);

    }, this);

    this.router.config.push({ path: '**', redirectTo: 'app/dashboard' });

  }
}


// class CategoryRoute {
//   public description: string;
//   public url: string;
//   public subject: SubjectRoute[];
// }
// class SubjectRoute {
//   public description: string;
//   public url: string;
// }

